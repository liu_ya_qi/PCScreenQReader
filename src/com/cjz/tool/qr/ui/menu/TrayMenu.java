package com.cjz.tool.qr.ui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;
import com.cjz.tool.qr.ui.MainFrame;

import craky.componentc.JCMenuItem;
import craky.componentc.JCPopupMenu;

public class TrayMenu extends JCPopupMenu {

	private static final long serialVersionUID = -886384837371013843L;
	JCMenuItem showItem, aboutItem, exitItem;

	public TrayMenu(final MainFrame mainFrame) {

		showItem = new JCMenuItem(Setting.STR_SETTING_SHOW);
		aboutItem = new JCMenuItem(Setting.STR_SETTING_ABOUT);
		exitItem = new JCMenuItem(Setting.STR_SETTING_FILE_EXIT);
		ActionListener listener = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				if (source == showItem) {
					mainFrame.reShow();
				} else if (source == exitItem) {
					QRreader.getInstance().exit();
				}
			}
		};

		showItem.addActionListener(listener);
		aboutItem.addActionListener(listener);
		exitItem.addActionListener(listener);

		add(showItem);
		addSeparator();
		add(aboutItem);
		addSeparator();
		add(mainFrame.getSettingMenuGroup());// 共用同一对象
		addSeparator();
		add(exitItem);

		addPopupMenuListener(new PopupMenuListener() {

			@Override
			public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
				mainFrame.getSettingMenuGroup().setCheckState();
				// 此处因共用同一对象,该对象需重设
				add(mainFrame.getSettingMenuGroup(), 4);
			}

			@Override
			public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

			}

			@Override
			public void popupMenuCanceled(PopupMenuEvent e) {

			}
		});
	}

}
