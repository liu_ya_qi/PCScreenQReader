package com.cjz.tool.qr.ui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;
import com.cjz.tool.qr.ui.MainFrame;

import craky.componentc.JCCheckBoxMenuItem;
import craky.componentc.JCMenu;

public class SettingGroup extends JCMenu {

	private static final long serialVersionUID = -5555997833506119824L;

	JCCheckBoxMenuItem cbmiKeepTop, cbmiHideSelf, cbmiMini2Tray, cbmiAutoCopy;

	public void setCheckState() {
		cbmiKeepTop.setSelected(Setting.alwaysTopWhenRunning);
		cbmiHideSelf.setSelected(Setting.hideSelfWhenCapture);
		cbmiMini2Tray.setSelected(Setting.minToTray);
		cbmiAutoCopy.setSelected(Setting.autoCopyResult);
	}

	public SettingGroup(final MainFrame mainFrame) {
		super(Setting.STR_SETTING_ADV);
		cbmiKeepTop = new JCCheckBoxMenuItem(Setting.STR_SETTING_ADV_ALWAYS_TOP_WHEN_RUNNING);
		cbmiHideSelf = new JCCheckBoxMenuItem(Setting.STR_SETTING_ADV_HIDE_SELF_WHEN_CAPTURE);
		cbmiMini2Tray = new JCCheckBoxMenuItem(Setting.STR_SETTING_ADV_MIN_TO_TRAY);
		cbmiAutoCopy = new JCCheckBoxMenuItem(Setting.STR_SETTING_ADV_AUTO_COPY_TO_SYS_CB);

		this.add(cbmiKeepTop);
		this.add(cbmiHideSelf);
		this.add(cbmiMini2Tray);
		this.add(cbmiAutoCopy);

		ActionListener listener = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Object source = e.getSource();
				if (source == cbmiHideSelf) {
					Setting.hideSelfWhenCapture = ((JCCheckBoxMenuItem) cbmiHideSelf).isSelected();
					QRreader.getInstance().saveConfig();
				} else if (source == cbmiKeepTop) {
					Setting.alwaysTopWhenRunning = ((JCCheckBoxMenuItem) cbmiKeepTop).isSelected();
					mainFrame.setAlwaysOnTop(Setting.alwaysTopWhenRunning);
					QRreader.getInstance().saveConfig();
				} else if (source == cbmiMini2Tray) {
					Setting.minToTray = ((JCCheckBoxMenuItem) cbmiMini2Tray).isSelected();
					QRreader.getInstance().saveConfig();
				} else if (source == cbmiAutoCopy) {
					Setting.autoCopyResult = ((JCCheckBoxMenuItem) cbmiAutoCopy).isSelected();
					QRreader.getInstance().saveConfig();
				}
			}

		};

		cbmiKeepTop.addActionListener(listener);
		cbmiHideSelf.addActionListener(listener);
		cbmiMini2Tray.addActionListener(listener);
		cbmiAutoCopy.addActionListener(listener);
	}

}
