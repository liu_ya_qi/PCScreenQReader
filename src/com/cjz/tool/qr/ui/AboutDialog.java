package com.cjz.tool.qr.ui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.net.URI;
import java.util.Calendar;
import java.util.Date;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import com.cjz.tool.qr.QRreader;
import com.cjz.tool.qr.config.Setting;

import craky.componentc.JCButton;
import craky.componentc.JCDialog;
import craky.componentc.JCLabel;
import craky.componentc.JCTextArea;
import craky.util.UIUtil;

public class AboutDialog extends JCDialog implements ActionListener {

	private static final long serialVersionUID = 260074972743789468L;
	private JCButton btnOk;

	public AboutDialog() {
		super(QRreader.getInstance().getMainFrame(), Setting.STR_ABOUT);
		setLocationRelativeTo(getOwner());
		initUI();
		setSize(300, 200);
		setLocation(QRreader.getInstance().getMainFrame().getX() + (QRreader.getInstance().getMainFrame().getWidth() - 300) / 2,
				(QRreader.getInstance().getMainFrame().getHeight() - 200) / 2 + QRreader.getInstance().getMainFrame().getY());
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setResizable(false);
		setVisible(true);
		this.addWindowFocusListener(new WindowFocusListener() {

			public void windowGainedFocus(WindowEvent e) {
			}

			public void windowLostFocus(WindowEvent e) {
				if (e!=null && SwingUtilities.isDescendingFrom(e.getOppositeWindow(), AboutDialog.this)) {
					return;
				}
				AboutDialog.this.setVisible(false);
			}

		});
	}

	private void initUI() {

		this.setLayout(new BorderLayout(5, 5));

		EmptyComponent ec = new EmptyComponent();
		ec.setBorder(new EmptyBorder(5, 5, 5, 5));
		ec.setLayout(new BorderLayout(5, 5));

		JCTextArea txtInfo = new JCTextArea();
		txtInfo.setAutoscrolls(true);
		txtInfo.setLineWrap(true);
		txtInfo.setImageOnly(true);
		txtInfo.clearBorderListener();
		txtInfo.setEditable(false);
		txtInfo.setPopupMenuEnabled(false);
		txtInfo.setFocusable(false);
		txtInfo.setCursor(new Cursor(0));
		txtInfo.setColumns(5);
		txtInfo.setText("《屏幕QR读取器 - V1.0》（以下简称 “本软件”），本软件使用完全免费，您可以对本软件任意复制分发，但禁止用于任何商业目的，严禁擅自对本软件进行更改、反向工程、反向编译及分解。由此带来的损失由用户自行承担。\n\n作者:cjz\n联系:cjztool@126.com");
		ec.add(txtInfo, BorderLayout.CENTER);

		JCLabel lbCopyright = new JCLabel("Copyright© 2010-" + (Calendar.getInstance().get(Calendar.YEAR) ) + ". All Rights Reserved");
		lbCopyright.setHorizontalAlignment(2);
		lbCopyright.setVerticalAlignment(1);
		lbCopyright.setBorder(null);
		lbCopyright.setFont(new Font("Tahoma", 0, 12));

		ec.add(lbCopyright, BorderLayout.SOUTH);
		this.add(ec);

		JCLabel lbVersion = new JCLabel("版本：V0.2");
		lbVersion.setHorizontalAlignment(2);
		lbVersion.setVerticalAlignment(0);
		lbVersion.setBorder(null);
		this.btnOk = new JCButton("确定(O)");
		this.btnOk.setPreferredSize(new Dimension(73, 21));
		this.btnOk.setMnemonic('O');
		this.btnOk.addActionListener(this);

		UIUtil.escAndEnterAction(this, this.btnOk, new AbstractAction() {
			public void actionPerformed(ActionEvent e) {
				AboutDialog.this.dispose();
			}
		});
	}

	private void mailToAuthor(ActionEvent e) {
		String mailTo = ((JCLabel) e.getSource()).getToolTipText();
		try {
			Desktop.getDesktop().mail(new URI("mailto:" + mailTo));
		} catch (Exception localException) {
		}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.btnOk) {
			dispose();
		}
	}
}
